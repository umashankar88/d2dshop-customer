package com.d2dshop.ui.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.d2dshop.utils.Logger;
import com.d2dshop.utils.Utility;

import io.reactivex.disposables.CompositeDisposable;

import static com.d2dshop.utils.Constants._action_auth_failed;


public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();
    private ProgressDialog progressDialog;

    protected CompositeDisposable mCompositeDisposable;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        disposeAllRxSubs();
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposeAllRxSubs();
    }

    /**
     * Dispose All Rx Subs.
     */
    private void disposeAllRxSubs() {
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed())
            mCompositeDisposable.dispose();
    }

    public void showProgressBar(ProgressBar progressBar) {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgressBar(ProgressBar progressBar) {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Method is used to hide progressbar
     **/
    public void hideProgressDialogue() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /**
     * Method is used for showing progressbar
     **/
    public void showProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            return;
        } else {
            progressDialog = Utility.showProgress(this, "Please wait..");
        }
    }

    private BroadcastReceiver mLogoutBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent == null) return;

            if (Utility.isValidString(intent.getAction())) {
                switch (intent.getAction()) {
                    case _action_auth_failed:
                        Utility.performLogout(BaseActivity.this);
                        break;
                }
            } else {
                Logger.e(TAG, "action not found");
            }
        }
    };

//    @Override
//    public void onStop() {
//        super.onStop();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLogoutBroadcast);
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        LocalBroadcastManager.getInstance(this).registerReceiver(mLogoutBroadcast, getIntentFilter());
//    }

    @NonNull
    private IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(_action_auth_failed);
        return intentFilter;
    }
}
