package com.d2dshop.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.d2dshop.R;
import com.d2dshop.utils.TypeFaceUtil;


public class CustomButtonView extends android.support.v7.widget.AppCompatButton {

    public CustomButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);

        if (isInEditMode()) {
            return;
        } else {
            Typeface typeface = TypeFaceUtil.setCustomTypeFace(context, a);
            if (typeface != null) {
                setTypeface(typeface);
            }
        }
    }
}
