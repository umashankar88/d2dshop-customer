package com.d2dshop.network;


import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.RequestBody;



public class ApiHelper {

    private static final String TAG = ApiHelper.class.getSimpleName();


    /**
     * perform sign up.
     *
     * @param signUpRequestBean
     * @return
     */

//
//    public static Observable<SignUpResponseBean> performSignUp(SignUpRequestBean signUpRequestBean) {
//        RequestBody name = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpRequestBean.getName());
//        RequestBody email = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpRequestBean.getEmail());
//        RequestBody mobile = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpRequestBean.getMobile());
//        RequestBody password = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpRequestBean.getPassword());
//        Map<String, RequestBody> map = new HashMap<>();
//        map.put(NetworkConstants.SIGNUP_PATH_PARAM_NAME, name);
//        map.put(NetworkConstants.SIGNUP_PATH_PARAM_PHONE, mobile);
//        map.put(NetworkConstants.SIGNUP_PATH_PARAM_EMAIL, email);
//        map.put(NetworkConstants.SIGNUP_PATH_PARAM_PASSWORD, password);
//        return DhowRestClient.getInstance().getRestAPIStore().performSignUp(map);
//    }
//
//    /**
//     * perform OTP Verification up.
//     *
//     * @param otpRequestBean
//     * @return
//     */
//
//
//    public static Observable<OtpResponseBean> performOTPVerification(OtpRequestBean otpRequestBean) {
//
//        RequestBody mobile = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), otpRequestBean.getMobile());
//        RequestBody mobileotp = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), otpRequestBean.getMobileotp());
//        Map<String, RequestBody> map = new HashMap<>();
//        map.put(NetworkConstants.OTP_PATH_PARAM_PHONE, mobile);
//        map.put(NetworkConstants.OTP_PATH_PARAM_OTP, mobileotp);
//
//        return DhowRestClient.getInstance().getRestAPIStore().performOTPVerify(map);
//
//    }
//
//
//    /**
//     * perform OTP Verification up.
//     *
//     * @param loginRequestBean
//     * @return
//     */
//
//
//    public static Observable<LoginResponseBean> performLogin(LoginRequestBean loginRequestBean) {
//
//        RequestBody emailOrMobile = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), loginRequestBean.getMobOremail());
//        RequestBody password = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), loginRequestBean.getPassword());
//        Map<String, RequestBody> map = new HashMap<>();
//        map.put(NetworkConstants.LOGIN_PATH_PARAM_EMAIL_OR_MOBILE, emailOrMobile);
//        map.put(NetworkConstants.LOGIN_PATH_PARAM_PASSWORD, password);
//
//        return DhowRestClient.getInstance().getRestAPIStore().performLogin(map);
//
//    }
//
//
//    /**
//     * perform sign up.
//     *
//     * @param signUpSocialRequestBean
//     * @return
//     */
//
//
//    public static Observable<SignUpSocialResponseBean> performSocialSignup(SignUpSocialRequestBean signUpSocialRequestBean) {
//
//        RequestBody username = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpSocialRequestBean.getUsername());
//        RequestBody oauth_uid = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpSocialRequestBean.getOauth_uid());
//        RequestBody oauth_provider = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpSocialRequestBean.getOauth_provider());
////        RequestBody mobile = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpSocialRequestBean.getMobile());
//        RequestBody email = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpSocialRequestBean.getEmail());
//        RequestBody profileimage = RequestBody.create(MediaType.parse(NetworkConstants.TEXT_PLAIN), signUpSocialRequestBean.getProfileimage());
//
//        Map<String, RequestBody> map = new HashMap<>();
//        map.put(NetworkConstants.SIGNUP_SOCIAL_PATH_PARAM_NAME, username);
//        map.put(NetworkConstants.SIGNUP_SOCIAL_PATH_PARAM_OUTHER, oauth_uid);
//
//        map.put(NetworkConstants.SIGNUP_SOCIAL_PATH_PARAM_PROVIDER, oauth_provider);
////        map.put(NetworkConstants.SIGNUP_SOCIAL_PATH_PARAM_PHONE, mobile);
//
//        map.put(NetworkConstants.SIGNUP_SOCIAL_PATH_PARAM_EMAIL, email);
//        map.put(NetworkConstants.SIGNUP_SOCIAL_PATH_PARAM_IMAGE, profileimage);
//
//        return DhowRestClient.getInstance().getRestAPIStore().performSocialSignup(map);
//    }
//
//
//    /**
//     * perform Dashboard.
//     *
//     *
//     */
//    public static Observable<DashBoardApiResponseBean> performDashboard(String id,String catid,String page) {
//        return DhowRestClient.getInstance().getRestAPIStore().performDashboard(id,catid,page);
//    }
//
//    /**
//     * perform Product List.
//     *
//     *
//     */
//
//    public static Observable<ProductListResponseBean> performProductList(String search_tag,String user_id,
//                                                                         String cat_id,int start,
//                                                                         int limit,String sort_by,
//                                                                         int min,int max,
//                                                                         String assured,String stock,
//                                                                         String sell_type) {
//        return DhowRestClient.getInstance().getRestAPIStore().
//                performProductList(search_tag,user_id,cat_id,start,limit,sort_by,
//                        min,max,assured,stock,sell_type);
//    }

}
