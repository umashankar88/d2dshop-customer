package com.d2dshop.network;


import com.d2dshop.BuildConfig;

public class NetworkConstants {

    static final int RETROFIT_API_SERVICE_SOCKET_TIMEOUT = 120;
    public static final int ION_MEDIA_UPLOAD_TIMEOUT    = 3 * 60 * 1000;

    /* HEADERS PARAMS*/
    public static final String HEADER_PARAM_KEY_TOKEN = "OuthKey";//OuthKey
    public static final String HEADER_PARAM_TOKEN_VALUE= "authorization";//authorization

    public static final String HEADER_PARAM_KEY_CONTENT_TYPE = "Content-Type";//Content-Type
    public static final String HEADER_PARAM_VALUE_CONTENT_TYPE_JSON = "application/json";// application/json
    public static final String HEADER_PARAM_VALUE_CONTENT_TYPE = "application/x-www-form-urlencoded";


    /* PATH PARAMS */
    static final String PATH_PARAM_USERNAME            = "username";
    static final String PATH_PARAM_PASSWORD            = "password";



    public static final String OTP_VERIFY_URL = BuildConfig.BASE_URL + "user/mobileVerify";
    public static final String LOGIN_URL = BuildConfig.BASE_URL + "user/login";
    public static final String SIGNUP_SOCIAL_URL=BuildConfig.BASE_URL+"user/socialLogin";
    public static final String REQUEST_TYPE = "POST";
    public static final int SUCCESS_RESPONSE_CODE_200 = 200;
    public static final int SUCCESS_RESPONSE_CODE_201 = 201;

    public static final String TEXT_PLAIN="text/plain";

     /*Signup Params*/

     public static final String SIGNUP_URL = BuildConfig.BASE_URL + "user/register";
    static final String SIGNUP_PATH_PARAM_NAME =        "name";
    static final String SIGNUP_PATH_PARAM_EMAIL =       "email";
    static final String SIGNUP_PATH_PARAM_PASSWORD =    "password";
    static final String SIGNUP_PATH_PARAM_PHONE =       "mobile";


   /* OTP Verfication*/

    static final String OTP_PATH_PARAM_OTP =    "mobileotp";
    static final String OTP_PATH_PARAM_PHONE =       "mobile";

     /*Signup using Social Params*/

    static final String SIGNUP_SOCIAL_PATH_PARAM_NAME =        "username";
    static final String SIGNUP_SOCIAL_PATH_PARAM_OUTHER =    "oauth_uid";
    static final String SIGNUP_SOCIAL_PATH_PARAM_PROVIDER =    "oauth_provider";
    static final String SIGNUP_SOCIAL_PATH_PARAM_PHONE =       "mobile";
    static final String SIGNUP_SOCIAL_PATH_PARAM_EMAIL =       "email";
    static final String SIGNUP_SOCIAL_PATH_PARAM_IMAGE =       "profileimage";

    /* Login*/

    static final String LOGIN_PATH_PARAM_EMAIL_OR_MOBILE =    "mobOremail";
    static final String LOGIN_PATH_PARAM_PASSWORD =       "password";

    /*Facebook Keys*/
    public static final String FACEBOOK_KEY_EMAIL="email";
    public static final String FACEBOOK_KEY_USER_BIRTHDAY="user_birthday";
    public static final String FACEBOOK_KEY_PUBLIC_PROFILE="public_profile";
    public static final String FACEBOOK_KEY_FIRST_NAME="first_name";
    public static final String FACEBOOK_KEY_LAST_NAME="last_name";
    public static final String FACEBOOK_KEY_FIELDS="fields";
    public static final String FACEBOOK_KEY_FIELDS_VALUE="first_name,last_name,email";

    public static final String FACEBOOK_KEY_PROFILE_URL1="http://graph.facebook.com/";
    public static final String FACEBOOK_KEY_PROFILE_URL2="/picture?type=large";
    public static final String FACEBOOK_ID_KEY="id";

    public static final String KEY_ID="id";

    public static final String PATH_PARMS_KEY_ID="id";
    public static final String PATH_PARMS_CAT_ID="catid";
    public static final String PATH_PARMS_SUB_CAT_ID="page";


    public static final String PATH_PARMS_PRODUCT_LIST_SEARCH_ID=       "search_tag";
    public static final String PATH_PARMS_PRODUCT_LIST_USER_ID=         "user_id";
    public static final String PATH_PARMS_PRODUCT_LIST_CATEGORIES_ID=   "cat_id";
    public static final String PATH_PARMS_PRODUCT_LIST_START=           "start";
    public static final String PATH_PARMS_PRODUCT_LIST_LIMIT=           "limit";
    public static final String PATH_PARMS_PRODUCT_LIST_SORT_BY=         "sort_by";
    public static final String PATH_PARMS_PRODUCT_LIST_MIN=             "min";
    public static final String PATH_PARMS_PRODUCT_LIST_MAX=             "max";
    public static final String PATH_PARMS_PRODUCT_LIST_ASSURED=         "assured";
    public static final String PATH_PARMS_PRODUCT_LIST_STOCK=           "stock";
    public static final String PATH_PARMS_PRODUCT_LIST_SELL_TYPE=       "sell_type";




    public static final String PATH_PARMS_CAT_ID_DEFULT_VALUE="0";
    public static final String PATH_PARMS_SUB_CAT_ID_DEFULT_VALUE="0";
    public static final String PATH_PARMS_SUB_CAT_ID_FIRST_VALUE="1";
//    public static final String PATH_PARMS_SUB_CAT_ID_SECOND_VALUE="2";





}
