package com.d2dshop.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;

import com.d2dshop.R;


public class TypeFaceUtil {

    public static final String FONT_FOLDER = "fonts/";//SourceSansPro

    public final static int SOURCESANSPRO_BLACK = 0;
    public final static int SOURCESANSPRO_BLACKITALIC = 1;
    public final static int SOURCESANSPRO_BOLD= 2;
    public final static int SOURCESANSPRO_BOLDITALIC = 3;
    public final static int SOURCESANSPRO_EXTRALIGHT= 4;
    public final static int SOURCESANSPRO_EXTRALIGHTITALIC= 5;
    public final static int SOURCESANSPRO_ITALIC= 6;
    public final static int SOURCESANSPRO_LIGHT= 7;
    public final static int SOURCESANSPRO_LIGHTITALIC= 8;
    public final static int SOURCESANSPRO_REGULAR = 9;
    public final static int SOURCESANSPRO_SEMIBOLD= 10;
    public final static int SOURCESANSPRO_SEMIBOLDITALIC = 11;


    public static Typeface typefaceSourcesansproBlack = null;
    public static Typeface typefaceSourcesansproBlackItalic = null;
    public static Typeface typefaceSourcesansproBold = null;
    public static Typeface typefaceSourcesansproBoldItalic = null;
    public static Typeface typefaceSourcesansproExtraLight = null;
    public static Typeface typefaceSourcesansproExtraLightItalic = null;
    public static Typeface typefaceSourcesansproItalic = null;
    public static Typeface typefaceSourcesansproLight = null;
    public static Typeface typefaceSourcesansproLightItalic = null;
    public static Typeface typefaceSourcesansproRegular = null;
    public static Typeface typefaceSourcesansproSemiBold = null;
    public static Typeface typefaceSourcesansproSemiBoldItalic = null;



    /**
     * Desc: This method is responsible to set custom font type
     *
     * @param context
     * @param a
     * @return
     */


    public static Typeface setCustomTypeFace(Context context, TypedArray a) {
        int typefaceIndex = a.getInt(R.styleable.CustomFontTextView_customFont, -1);

        Typeface tf = null;
        String fontFileName = null;
        AssetManager assetManager = context.getResources().getAssets();

        try {
            switch (typefaceIndex) {
                case SOURCESANSPRO_BLACK:
                    fontFileName = FONT_FOLDER + "SourceSansPro-Black.ttf";
                    if (typefaceSourcesansproBlack == null) {
                        typefaceSourcesansproBlack = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproBlack;
                    break;
                case SOURCESANSPRO_BLACKITALIC:
                    fontFileName = FONT_FOLDER + "SourceSansPro-BlackItalic.ttf";
                    if (typefaceSourcesansproBlackItalic == null) {
                        typefaceSourcesansproBlackItalic = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproBlackItalic;
                    break;
                case SOURCESANSPRO_BOLD:
                    fontFileName = FONT_FOLDER + "SourceSansPro-Bold.ttf";
                    if (typefaceSourcesansproBold == null) {
                        typefaceSourcesansproBold = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproBold;
                    break;
                case SOURCESANSPRO_BOLDITALIC:
                    fontFileName = FONT_FOLDER + "SourceSansPro-BoldItalic.ttf";
                    if (typefaceSourcesansproBoldItalic == null) {
                        typefaceSourcesansproBoldItalic = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproBoldItalic;
                    break;
                case SOURCESANSPRO_EXTRALIGHT:
                    fontFileName = FONT_FOLDER + "SourceSansPro-ExtraLight.ttf";
                    if (typefaceSourcesansproExtraLight == null) {
                        typefaceSourcesansproExtraLight = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproExtraLight;
                    break;
                case SOURCESANSPRO_EXTRALIGHTITALIC:
                    fontFileName = FONT_FOLDER + "SourceSansPro-ExtraLightItalic.ttf";
                    if (typefaceSourcesansproExtraLightItalic == null) {
                        typefaceSourcesansproExtraLightItalic = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproExtraLightItalic;
                    break;
                case SOURCESANSPRO_ITALIC:
                    fontFileName = FONT_FOLDER + "SourceSansPro-Italic.ttf";
                    if (typefaceSourcesansproItalic == null) {
                        typefaceSourcesansproItalic = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproItalic;
                    break;
                case SOURCESANSPRO_LIGHT:
                    fontFileName = FONT_FOLDER + "SourceSansPro-Light.ttf";
                    if (typefaceSourcesansproLight == null) {
                        typefaceSourcesansproLight = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproLight;
                    break;
                case SOURCESANSPRO_LIGHTITALIC:
                    fontFileName = FONT_FOLDER + "SourceSansPro-LightItalic.ttf";
                    if (typefaceSourcesansproLightItalic == null) {
                        typefaceSourcesansproLightItalic = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproLightItalic;
                    break;
                case SOURCESANSPRO_REGULAR:
                    fontFileName = FONT_FOLDER + "SourceSansPro-Regular.ttf";
                    if (typefaceSourcesansproRegular == null) {
                        typefaceSourcesansproRegular = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproRegular;
                    break;
                case SOURCESANSPRO_SEMIBOLD:
                    fontFileName = FONT_FOLDER + "SourceSansPro-SemiBold.ttf";
                    if (typefaceSourcesansproSemiBold == null) {
                        typefaceSourcesansproSemiBold = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproSemiBold;
                    break;
                case SOURCESANSPRO_SEMIBOLDITALIC:
                    fontFileName = FONT_FOLDER + "SourceSansPro-SemiBoldItalic.ttf";
                    if (typefaceSourcesansproSemiBoldItalic == null) {
                        typefaceSourcesansproSemiBoldItalic = Typeface.createFromAsset(assetManager, fontFileName);
                    }
                    tf = typefaceSourcesansproSemiBoldItalic;
                    break;

            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return tf;
    }
}
