package com.d2dshop.utils;

/**
 * Created by prashant on 19/3/18.
 */

public interface KeyboardHeightObserver {

    void onKeyboardHeightChanged(int height, int orientation);
}
