package com.d2dshop.utils;


public class Constants {

    public static final String DEVICE_TYPE = "Android";
    public static final String DEVICE_TOKEN = "Android123";
    public static final String NETWORK_TAG = "network";
    public static final String MESSAGE_IS_NULL = "Message is null.";


    public static final String INTENT_EXTRA_DATA = "intent_extra_data";

    /* actions */
    public static final String _action_auth_failed = "action.do.auth.failed";

    public static final int SPLASH_TIMEOUT = 3000;
    public static final int HOW_TO_WORK_SCREEN_MOVING_AUTOMATICALLY_TIME =7000;

    public static final String INTENT_OTP_MOBILE_KEY="mobile";
    public static final int KEY_MIN_VALUE=0;
    public static final int KEY_MAX_VALUE=0;

    public static final int START_PAGE = 0;
    public static final int PAGE_LIMIT = 10;








}
