package com.d2dshop.utils;


import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.text.TextUtils;
import android.util.Log;

import com.d2dshop.BuildConfig;

import static com.d2dshop.utils.Constants.MESSAGE_IS_NULL;
import static com.d2dshop.utils.Constants.NETWORK_TAG;


public class Logger {


    public static void e(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.e(tag, message);
        }
    }

    public static void d(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.d(tag, message);
        }
    }

    public static void i(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.i(tag, message);
        }
    }


    public static void w(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.w(tag, message);
        }
    }


    public static void v(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Log.v(tag, message);
        }
    }

    /**
     * <b>Log Network Event</b>
     * Log network call request response event.
     * All log will be logged as error and with tag <b><i>network</i></b>.
     *
     * @param message
     */
    public static void logNetworkEvent(@NonNull String message) {
        if (BuildConfig.LOGGING) {
            if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
            Logger.e(NETWORK_TAG, message);
        }
    }

    /**
     * Log event without caring about <b>Build Type</b>.
     *
     * @param tag
     * @param message
     */
    public static void logIndependentEvent(@NonNull @Size(min = 1, max = 23) String tag, @NonNull String message) {
        if (TextUtils.isEmpty(message)) message = MESSAGE_IS_NULL;
        Log.e(tag, message);
    }

}
