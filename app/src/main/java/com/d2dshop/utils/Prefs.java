package com.d2dshop.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.d2dshop.app.D2DShop;
import com.d2dshop.model.User;
import com.google.gson.Gson;


public class Prefs {

    private static Prefs prefs;
    private SharedPreferences sharedPreferences;

    private Prefs() {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(D2DShop.getContext());
    }

    public static Prefs instanceOf() {
        if (prefs == null) {
            prefs = new Prefs();
        }

        return prefs;
    }

    public void saveIsFirstTimeLaunch(boolean flag) {

        putDataWithKey(Keys.IS_FIRST_TIME_LAUNCH, flag);
    }

    public boolean isFirstTimeLaunch() {

        return getDataWithKey(Keys.IS_FIRST_TIME_LAUNCH, true);
    }


    /**
     * Get data from prefs with key {key} & of type {obj}
     *
     * @param key
     * @param obj
     * @param <T>
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T> T getDataWithKey(String key, T obj) {
        if (obj instanceof String) {
            return (T) sharedPreferences.getString(key, (String) obj);
        } else if (obj instanceof Integer) {
            return (T) (Integer) sharedPreferences.getInt(key, (Integer) obj);
        } else if (obj instanceof Boolean) {
            return (T) (Boolean) sharedPreferences.getBoolean(key, (Boolean) obj);
        }
        return null;
    }

    /**
     * Save data to prefs with key {key} & of type {obj}
     *
     * @param key
     * @param obj
     * @param <T>
     * @return
     */
    public <T> void putDataWithKey(String key, T obj) {
//        Logger.e("Key:"+key,"Value:"+obj);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (obj instanceof String) {
            editor.putString(key, (String) obj);
        } else if (obj instanceof Integer) {
            editor.putInt(key, (Integer) obj);
        } else if (obj instanceof Boolean) {
            editor.putBoolean(key, (Boolean) obj);
        }else if (obj instanceof Float) {
            editor.putFloat(key, (Float) obj);
        }
        editor.commit();
    }

    /**
     * Get User Token
     *
     * @return
     */
    public String getToken() {
        return getDataWithKey(Keys.TOKEN, "");
    }

    /**
     * Save User Id to prefs.
     *
     * @param token
     */
    public void setToken(String token) {
        putDataWithKey(Keys.TOKEN, token);
    }





    /**
     * Get User Id
     *
     * @return
     */
    public String getUserId() {
        return getDataWithKey(Keys.USER_ID, "");
    }




    /**
     * Save User Token to prefs.
     *
     * @param userId
     */
    public void setUserId(String userId) {
        putDataWithKey(Keys.USER_ID, userId);
    }

    public void setUser(User user) {
        putDataWithKey(Keys.USER_OBJ, new Gson().toJson(user));
    }

    public User getUser() {

        String userString = getDataWithKey(Keys.USER_OBJ, "");

        if (!TextUtils.isEmpty(userString)) {
            return new Gson().fromJson(userString, User.class);
        }

        return null;
    }

    public void setLoginIn(boolean isLogin) {
        putDataWithKey(Keys.IS_LOGIN, isLogin);
    }

    public boolean isLoggedIn() {
        return getDataWithKey(Keys.IS_LOGIN, false);
    }

    public void clearPref() {
        sharedPreferences.edit().clear().commit();
    }


    /**
     * Get User Token
     *
     * @return
     */
    public String getUserImage() {
        return getDataWithKey(Keys.USER_AVATAR, "");
    }

    /**
     * Save User Id to prefs.
     *
     * @param token
     */
    public void setUserImage(String token) {
        putDataWithKey(Keys.USER_AVATAR, token);
    }


    /**
     * Save User Token to prefs.
     *
     * @param userName
     */
    public void setUserName(String userName) {
        putDataWithKey(Keys.USER_NAME, userName);
    }

    public String getUserName() {
        return getDataWithKey(Keys.USER_NAME, "");
    }

    /**
     * This Class will contains all keys going to use in preferences.
     */
    public static class Keys {

        static final String IS_LOGIN = "prefs_is_login";
        static final String TOKEN = "auth_token";
        static final String USER_ID = "user_id";
        static final String USER_NAME = "user_name";
        static final String USER_OBJ = "user";
        static final String USER_AVATAR = "user_avatar";
        static final String IS_FIRST_TIME_LAUNCH="is_first_time_launch";

    }
}
