package com.d2dshop.utils;

import android.graphics.Bitmap;

import com.d2dshop.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;


public class ImageUtils {
    private static final String TAG = ImageUtils.class.getSimpleName();

    /**
     * display option for order listing.
     *
     * @return
     */
    public static DisplayImageOptions getImageDisplayOptions() {
        return new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnFail(R.drawable.placeholder_na_image)
                .showImageForEmptyUri(R.drawable.placeholder_na_image)
                .showImageOnLoading(R.drawable.placeholder_na_image)
                .considerExifParams(true)
                .build();
    }


    /**
     * display option for order listing.
     *
     * @return
     */
    public static DisplayImageOptions getImageDisplayOptionsUserProfile() {
        return new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnFail(R.drawable.profilelogo)
                .showImageForEmptyUri(R.drawable.profilelogo)
                .showImageOnLoading(R.drawable.profilelogo)
                .considerExifParams(true)
                .build();
    }


}
