package com.d2dshop.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.d2dshop.R;
import com.d2dshop.ui.activity.BaseActivity;

import static android.provider.Settings.System.getString;
import static com.d2dshop.app.D2DShop.getContext;


public class Utility {

    public static void fullScreenSize(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = activity.getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
    }

    public static ProgressDialog showProgress(Context context, String message) {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        if (!((Activity) context).isDestroyed())
        progressDialog.show();
        return progressDialog;
    }

    /* * Check whether network is available or not.
     *
             * @param isShowMessage Is to be show a description, If network not connected.
            * @return If connected to an active network returns true, otherwise false.
            */

    public static boolean isNetworkAvailable(boolean isShowMessage) {

        boolean isConnected;
        Context context = getContext();

        if (context == null) return false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();

        isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnected();

        if (!isConnected && isShowMessage) {
            showMessageS(context.getString(R.string.no_network));
        }

        return isConnected;

    }



    /**
     * Show Short toast Message.
     *
     * @param message description to be shown.
     */
    @UiThread
    public static void showMessageS(@NonNull String message) {

        if (TextUtils.isEmpty(message)) return;

        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    /**
     * Show Long Toast Message.
     *
     * @param message description to be shown.
     */
    @UiThread
    public static void showMessageL(@NonNull String message) {

        if (TextUtils.isEmpty(message)) return;

        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

    }

    /***For Dismissing Progress***/
    public static boolean dismissProgress(Context context, ProgressDialog progressDialog) {

        if (context instanceof Activity) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (((Activity) context).isDestroyed()) {
                return true;
            }

        }
        return false;
    }

    /**
     * Get Device Id.
     *
     * @return
     */
    public static String getDeviceId() {
        if (getContext() == null) return "";
        return getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isValidString(String string) {
        return !TextUtils.isEmpty(string);
    }


    /* Validation of Email */
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /*Set Editable false on editbox*/
    public static void setEditTableFalse(EditText edt, boolean flag){
        edt.setFocusable(flag);
        edt.setFocusableInTouchMode(flag); // user touches widget on phone with touch screen
        edt.setClickable(flag);
    }

   /* Logout Alert Dialog*/
    public static void showAlertLogoutAlert(final Activity activity){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.AppCompatAlertDialogStyle);
        builder.setMessage(R.string.are_your_sure_want_to_logout)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        performLogout(activity);

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public static void launchHomeActivity(Activity context) {
//        Intent intent = new Intent(context, DashboardActivity.class);
//        context.startActivity(intent);
//        context.overridePendingTransition(R.anim.enter_activity, R.anim.exit_activity);
//        context.finishAffinity();
    }


    public static void performLogout(Activity activity) {
        Prefs.instanceOf().clearPref();
        showMessageS(activity.getString(R.string.logout_success));
        activity.finishAffinity();
        launchHomeActivity(activity);

    }


    public static void launchActivity(Context context, Class<? extends BaseActivity> activityClass){
        context.startActivity(new Intent(context,activityClass));
    }


    public static void launchActivity(Context context, Class<? extends BaseActivity> activityClass, Bundle bundle){
        Intent intent=new Intent(context,activityClass);
        if(bundle!=null){
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }

    public static SharedPreferences preferencesFirstTimeLaunch;
    private static Utility utility;

    private Utility(Context context) {
        preferencesFirstTimeLaunch = context.getSharedPreferences(context.getPackageName(), 0);
    }

    public static Utility instanceOf(Context context) {
        if (utility == null) {
            utility = new Utility(context);
        }

        return utility;
    }

    public static void saveFirstLaunch(boolean flag){
        preferencesFirstTimeLaunch.edit().putBoolean("firstime",flag).commit();
    }

    public static boolean isFirstTimeLaunch(){

        return preferencesFirstTimeLaunch.getBoolean("firstime",true);
    }



}
