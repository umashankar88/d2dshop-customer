package com.d2dshop.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ResponseBean<T> {

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("response")
    @Expose
    private T response;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return response;
    }

    public void setData(T data) {
        this.response = data;
    }




}
