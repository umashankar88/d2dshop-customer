package com.d2dshop.app;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.d2dshop.BuildConfig;
import com.d2dshop.utils.Logger;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by umashankar on 02/05/18.
 */

public class D2DShop extends MultiDexApplication {

    private static final String TAG = D2DShop.class.getSimpleName();
    private static Context mApplicationContext;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationContext = this;

        MultiDex.install(this);


        initImageLoader();

        setupBuildInfo();

    }

    /**
     * Get Application Context.
     *
     * @return
     */
    public static Context getContext() {
        return mApplicationContext;
    }

    /**
     * This will initialize image loader for applicaton.
     */
    private void initImageLoader() {
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoaderConfiguration configuration = ImageLoaderConfiguration.createDefault(this);
        if (!imageLoader.isInited())
            imageLoader.init(configuration);

    }

    /**
     * Just to display app info
     */
    private void setupBuildInfo() {
        Logger.i(TAG, "Flavor : " + BuildConfig.FLAVOR +
                "\nBuild version code : " + BuildConfig.VERSION_CODE +
                "\nBuild version name : " + BuildConfig.VERSION_NAME);
        Logger.i(TAG, "Base Url : " + BuildConfig.BASE_URL);
    }


}

